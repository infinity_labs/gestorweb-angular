import { GestorWebPage } from './app.po';

describe('gestor-web App', () => {
  let page: GestorWebPage;

  beforeEach(() => {
    page = new GestorWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
