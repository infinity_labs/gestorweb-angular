import { browser, element, by } from 'protractor';

export class InternalServerErrorPage {
  navigateTo() {
    return browser.get('/500');
  }

  getParagraphText() {
    return element(by.css('app-internal-server-error p')).getText();
  }
}
