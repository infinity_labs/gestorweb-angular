import { PageNotFoundPage } from './page-not-found.po';

describe('page not found', () => {
    let page: PageNotFoundPage;

    beforeEach(() => {
        page = new PageNotFoundPage();
    });

    it('should display message saying page-not-found works', () => {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('page-not-found works!');
    });
});
