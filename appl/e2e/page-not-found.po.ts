import { browser, element, by } from 'protractor';

export class PageNotFoundPage {
  navigateTo() {
    return browser.get('/404');
  }

  getParagraphText() {
    return element(by.css('app-page-not-found p')).getText();
  }
}
