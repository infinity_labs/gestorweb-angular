import { InternalServerErrorPage } from './internal-server-error.po';

describe('internal server error', () => {
    let page: InternalServerErrorPage;

    beforeEach(() => {
        page = new InternalServerErrorPage();
    });

    it('should display message saying internal-server-error works', () => {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('internal-server-error works!');
    });
});
