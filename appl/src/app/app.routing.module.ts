import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InternalServerErrorComponent } from './internal-server-error/internal-server-error.component';

const appRouting: Routes = [
    { path: '404', component: PageNotFoundComponent },
    { path: '500', component: InternalServerErrorComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(appRouting)],
    exports: [RouterModule],
    providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
})
export class AppRoutingModule {

}
